<!DOCTYPE html>
<html lang="en">

    
<?php 

?>

<head>
    <title>Login Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    
    <div class="wrapper">
        <div class="headline">


            <h1>My life, my diet</h1>
        </div>
        <!-- Inscription-->
        <form class="form" method="post" action="../controller/connexion_inscription.php">
            <div class="signup">
                <div class="form-group">
                    <input class="pseudo_ins" type="text" placeholder="Nom" required="" name="nom_ins"
                        id="nom_ins">
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Prenom" required name="prenom_ins" id="prenom_ins">
                </div>

                <div class="form-group">
                    <input type="date"  required="" name="date_ins" id="date_ins">
                </div>
 
                <div class="form-group">
                <?php  
                if($_GET['error']){
                        echo "<label style='color:#d62525'>".$_GET['error']."</label>";
                    }
                    ?>
                    <input type="email" placeholder="Email" required="" name="mail_ins" id="mail_ins">
                </div>
                <div class="form-group" id="passwordForm1">
                    <input type="password" placeholder="Password" required="" name="mdp_ins" id="mdp_ins">
                    <div class="test">
                        <div class="show.hide">
                            <i class="fa-solid fa-eye" id="togglePassword" style=" cursor: pointer;"></i>
                        </div>
                    </div>
                </div>
              <div>

                </div>
                <button type="submit" class="btn">SIGN UP</button>
                <div class="account-exist">
                    Vous avez déjà un compte? <a href="#" id="login">Login</a>
                </div>
            </div>
        </form>


        <!-- connexion-->

        <form class="form" method="post" action="../controller/connexion_control.php">
            <div class="signin">
                <div class="form-group">
                <?php 
                    if($_GET['test']){
                        echo "<label style='color:#d62525'>".$_GET['test']."</label>";
                    }
                    ?>
                    <input type="text" placeholder="Email" required="" name="email_con" id="email_con">
                </div>
                <div class="form-group" id="passwordForm">
                    <input type="password" placeholder="Password" required="" name="mdp_con" id="mdp_con">
                    <div class="test">
                        <div class="show.hide">
                            <i class="fa-solid fa-eye" id="togglePassword2" style=" cursor: pointer;"></i>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn">LOGIN</button>
       
       
        <div class="account-exist">
            Créer un nouveau compte? <a href="#" id="signup">Signup</a>
        </div>
        
    </div>
    </form>
    </div>

    

    <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
    <script src="app.js"></script>

</body>

</html>