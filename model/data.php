<?php

include "db_connect.php";


function addCoordonneesClient($nom,$prenom,$date,$email,$mdp, $photo_profil) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO utilisateur (nom ,prenom, date_de_naissance,email, mdp ,photo_profil) VALUES(?,?,?,?,?,?)');
    $req->execute([$nom,$prenom,$date,$email,$mdp, $photo_profil]);
};    


function CheckExistClientMail($mail){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM utilisateur WHERE email=?");
    $stmt->execute([$mail]);
    $mail = $stmt->fetch();
    if ($mail) {
        return true;
    } else {
        return false;
    }
};


function selectClientemail($email){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE email = ? ');
    $req->execute([$email]);
    return $req->fetchAll();
};



function insertIMC($weight,$height,$imc,$body) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO IMC (weightIMC ,heightIMC, imc,body, dateIMC) VALUES(?,?,?,?,NOW());")');
    $req->execute([$weight,$height,$imc, $body]);
    return $pdo->lastInsertId();
};    

function insertCalories($poids,$taille,$age,$sex,$actif,$result) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO calories (weightCAL ,heightCAL, age,sex, activité, result,dateCalories) VALUES(?,?,?,?,?,?,NOW());")');
    $req->execute([$poids,$taille,$age,$sex,$actif,$result]);
    return $pdo->lastInsertId();
};    



function liaison($IDclient, $id_imc ,$id_cal ){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO liaison(id_utilisateur,id_IMC ,id_calories) VALUES(?,?,?)');
    $req->execute([$IDclient, $id_imc,$id_cal]); 
}


function all($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM liaison   INNER JOIN IMC ON liaison.id_IMC = IMC.id INNER JOIN calories ON liaison.id_calories = calories.id INNER JOIN utilisateur ON liaison.id_utilisateur = utilisateur.id  WHERE id_utilisateur = ?  ');
   $req->execute([$id]);
    return $req->fetchAll();
};


function help($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM liaison INNER JOIN calories ON liaison.id_calories = calories.id INNER JOIN utilisateur ON liaison.id_utilisateur = utilisateur.id  WHERE id_utilisateur = ? ORDER BY id_calories DESC LIMIT 1 ');
    $req->execute([$id]);
    return $req->fetchAll();

}

function all10($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM liaison   INNER JOIN IMC ON liaison.id_IMC = IMC.id INNER JOIN calories ON liaison.id_calories = calories.id INNER JOIN utilisateur ON liaison.id_utilisateur = utilisateur.id  WHERE id_utilisateur = ? ORDER BY dateIMC AND dateCalories  DESC LIMIT 10 ');
   $req->execute([$id]);
    return $req->fetchAll();
};


function selectClientId($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE id = ? ');
    $req->execute([$id]);
    return $req->fetchAll();
};

function updateClient($column, $rep, $id ){
    global $pdo;
    $req = $pdo->prepare('UPDATE utilisateur
    SET '.$column.' = ? WHERE id = ?');
    $req->execute([$rep, $id ]);
   
}

function deleteClient($effacer){
    global $pdo;
    try{
        $req = $pdo->prepare('DELETE FROM utilisateur WHERE id = ?');
        $req->execute([$effacer]); 

    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    }
};