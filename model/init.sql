DROP DATABASE IF EXISTS My_life_my_diet;
CREATE DATABASE My_life_my_diet;
USE My_life_my_diet;


CREATE TABLE utilisateur (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(120),
    prenom LONGTEXT,
    date_de_naissance DATE,
    email VARCHAR(270),
    mdp VARCHAR(270),
    photo_profil VARCHAR(255)
);

CREATE TABLE IMC (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    weightIMC VARCHAR(120),
    heightIMC VARCHAR(270),
    imc FLOAT,
    body VARCHAR(270),
    dateIMC DATE
);

CREATE TABLE calories (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    weightCAL VARCHAR(120),
    heightCAL VARCHAR(270),
    age VARCHAR(270),
    sex VARCHAR(270),
    activité VARCHAR(270),
    result FLOAT,
    dateCalories DATE
);


CREATE TABLE liaison (
    id_utilisateur INT UNSIGNED,
    id_IMC INT UNSIGNED,
    id_calories INT UNSIGNED,
    UNIQUE(id_IMC,id_calories),
    FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id) ON DELETE CASCADE,
    FOREIGN KEY (id_IMC) REFERENCES IMC(id),
    FOREIGN KEY (id_calories) REFERENCES calories(id)
    
);