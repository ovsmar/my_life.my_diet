<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../header/header.css">
    <title>Header</title>
</head>
<body>
<header class="header" id="header">
	<nav class="navbar container">
		<a href="./index.html" class="brand">My life, my diet</a>
		<div class="menu" id="menu">
			<ul class="menu-list">
				<li class="menu-item">
					<a href="#" class="menu-link is-active">
						<i class="menu-icon ion-md-home"></i>
						<span class="menu-name">Home</span>
					</a>
				</li>
				<li class="menu-item">
					<a href="../connexion/Sing-up-login.php" class="menu-link">
						<i class="menu-icon ion-md-search"></i>
						<span class="menu-name">Inscription</span>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</header>
<script src="../header/header.js"></script>

