<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../header/header.css">
    <title>Header</title>
</head>
<body>
<header class="header" id="header">
	<nav class="navbar container">
		<a href="../home/index.php" class="brand">My life, my diet</a>
		<div class="menu" id="menu">
			<ul class="menu-list">
				<li class="menu-item">
					<a href="../home/index.php" class="menu-link is-active">
						<i class="menu-icon ion-md-home"></i>
						<span class="menu-name">Home</span>
					</a>
				</li>
				
				<li class="menu-item">
					<a href="../client/espace_client.php" class="menu-link">
						<i class="menu-icon ion-md-cart"></i>
						<span class="menu-name">calculateur</span>
					</a>
				</li>
				<li class="menu-item">
					<a href="../client/archiveModif.php" class="menu-link">
						<i class="menu-icon ion-md-heart"></i>
						<span class="menu-name">Archive/modif</span>
					</a>
				</li>
				<li class="menu-item">
					<a href="../sessiondelete/sessiondelete.php" class="menu-link">
						<i class="menu-icon ion-md-search"></i>
						<span class="menu-name">Deconnexion</span>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</header>
<script src="header.js"></script>

