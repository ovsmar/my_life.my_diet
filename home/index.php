
<?php include "../header/header.php"; ?>
 
    <link rel="stylesheet" href="index.css">

    <div class ="bannerhead">
    <div class="overlay">
    <h1 class="title font-styles">MY LIFE, MY DIET</h1>
	</div>
</div>

<div class="container1">
    <div class="text">
        <p class="koho"> En tant que discipline scientifique, l’écologie s’appuie sur les progrès continus de la
            connaissance au cours de la fin du XIXe siècle puis du XXe, et se documente au carrefour de toutes les
            disciplines liées de près ou de loin à la biologie, telles que la génétique, l’éthologie ou encore la
            géologie et la climatologie. Son objectif est principalement de comprendre la complexité des écosystèmes
            naturels. La discipline va continuer à se développer jusqu’à aujourd’hui.
            Mais l’écologie devient aussi une idée politique : les prémices de l’écologie politique apparaissent
            également en Occident, dès la deuxième moitié du XIXe siècle, en réaction à l’avènement de l’ère
            industrielle. Face aux pollutions et aux dégradations de la nature engendrées par les activités économiques
            et industrielles, de plus en plus d’individus vont commencer à revendiquer une forme de protection
            nécessaire de la nature. Cette idée va se développer tout au long du 20ème siècle mais c’est surtout à
            partir des années 1960 que ce courant de pensée, va devenir un vrai mouvement politique.
            En France, c’est à peu près à cette époque que des voix commencent à s’élever pour faire de l’écologie un
            projet politique. En 1961 est fondée l’ONG WWF, puis en 1970 ce sont les Amis de la Terre. Dans le même
            temps, dès 1974, l’élection présidentielle française est marquée par la première participation d’un candidat
            ouvertement écologiste : René Dumont. Dans le monde, c’est aussi à cette période que l’idée de l’écologie
            politique se développe. Aux Etats-Unis, la publication du livre Silent Sprint par Rachel Carson en 1962
            alerte sur les dangers de la crise écologique. S’en suit en 10 ans plus tard la publication du Rapport
            Meadows (Halte à la Croissance) par des chercheurs du MIT mettant en évidence les dangers écologiques de la
            croissance économique mondiale. Depuis, la prise de conscience écologique n’a cessé de se développer.</p>
    </div>
</div>

<!-- <div class="containerofImage">
    <div class="image">
        <img class= img1 src = "../stock/diana-polekhina-OpB7gkQY9oc-unsplash.jpg" alt ="">
    </div>
</div> -->


<div class="banner2">
    <div class="banner-me">
        <div class="text-box-wrapper">
            <h4 class="title">Calculez </h4>
            <h3 class="title2"> votre Indice de Masse Corporelle</h3>
            <h3 class="title3"> et votre apport calorique!</h3>
            <a class="banner-button" href="../connexion/Sing-up-login.php">Essayer</a>
        </div>
    </div>
</div> 


<?php include "../footer/footer.php"; ?>

