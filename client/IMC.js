// Define variables for range sliders
// Weight variables
let weightSlider = document.getElementById("myWeight");
let weightOutput = document.getElementById("inputWeight");
// Height variables
let heightSlider = document.getElementById("myHeight");
let heightOutput = document.getElementById("inputHeight");
//div
let targetDiv = document.getElementById("global");

let a = "On considère que la personne est maigre";
let b =
  "On considère que la personne a une corpulence <strong>normale</strong>";
let c = "On considère que la personne est en <strong>surpoids</strong>";
let d = "On considère que la personne est en <strong>obésité modérée</strong>";
let e = "On considère que la personne est en <strong>obésité sévère</strong>";
let f = "On considère que la personne est en <strong>obésité morbide</strong>";

//les images
let image_maigre = "../stock/18IMC-removebg-preview.png";
let image_normale = "../stock/25IMC-removebg-preview.png";
let image_surpoids = "../stock/30IMC-removebg-preview.png";
let image_obésité_modérée = "../stock/40IMC-removebg-preview.png";
let image_obésité_sévère = "../stock/plus40IMC-removebg-preview.png";
let image_obésité_morbide = "../stock/plus40IMC-removebg-preview.png";

// Calculate IMC
function calculateBmi() {
  if (targetDiv.style.display !== "none") {
    console.log("ok");
    targetDiv.style.display = "none";
  } else {
    targetDiv.style.display = "block";
  }

  var weight = document.bmiForm.realweight.value;
  var height = document.bmiForm.realheight.value / 100;
  var realbmi = weight / Math.pow(height, 2);
  var realbmiOutput = document.getElementById("yourbmi");
  var imcSQL = document.getElementById("imcSQL");
  var messageOutput = document.getElementById("evaluationMessage");
  var bodySQL = document.getElementById("bodySQL");
  var image = document.getElementById("image");
  var roundedBmi = realbmi.toFixed(1);

  messageOutput.innerHTML = ""; // Clear message before calculating new BMI
  bodySQL.innerHTML = "";
  realbmiOutput.innerHTML = "<strong> IMC: </strong>" + " " + roundedBmi; // Print BMI

  imcSQL.value = roundedBmi;

  // pour la balise image
  let x = '<img class ="imgjs" src="';
  let y = '"/>';

  let alert = document.getElementById("alert");
  let m = `<div class="popup">
  <p>You need help ? contact us: <a href="tel:123-456-7890">123-456-7890</a></p>
</div>`;

  alert.innerHTML = "";

  if (roundedBmi < 18.4) {
    messageOutput.innerHTML = a;
    bodySQL.value = a;
    image.innerHTML = x + image_maigre + y;
    alert.innerHTML = m;
  } else if (roundedBmi < 24.9) {
    bodySQL.value = b;
    messageOutput.innerHTML = b;
    image.innerHTML = x + image_normale + y;
  } else if (roundedBmi < 29.9) {
    bodySQL.value = c;
    messageOutput.innerHTML = c;
    image.innerHTML = x + image_surpoids + y;
  } else if (roundedBmi < 34.9) {
    bodySQL.value = d;
    messageOutput.innerHTML = d;
    image.innerHTML = x + image_obésité_modérée + y;
  } else if (roundedBmi < 39.9) {
    bodySQL.value = e;
    messageOutput.innerHTML = e;
    image.innerHTML = x + image_obésité_sévère + y;
  } else if (roundedBmi > 40) {
    bodySQL.value = f;
    messageOutput.innerHTML = f;
    image.innerHTML = x + image_obésité_sévère + y;
    alert.innerHTML = m;
  }
}
