<?php
session_start();

include "../debug/debug.php";
include '../model/data.php';

if(
    isset($_POST['email']) !==""&&
    isset($_POST['mdp']) !=="" 
    )
  $email = $_POST['email']; 
  $pwd = $_POST["mdp"];
  if(password_verify($pwd,selectClientemail($email)[0]["mdp"]) === true){
    echo 'client';
    session_destroy();
    unlink(selectClientemail($email)[0]["photo_profil"]);
    deleteClient(selectClientemail($email)[0]["id"]); 
    header('Location: ../home/index.php');
  }else{
    echo '<p style="color : red">Mot de passe invalide</p>';
  }